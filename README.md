# CarCar

Team:

* Aden - Service
* Micheline - Sales

## Design
In the design, there are three bounded contexts, Inventory, Sales and Service.  Each with their own aggregate routes, sales and service, to Inventory. 
In it's own context, inventory is connected to: manufacturer, vehicle model, and automobiles, which each have their own connections to such things as - name, year, vin, etc.  They are also connected to each other as manufacturer and vehicle model both get referenced in automobiles.

## Service microservice

I have created a few models - AutomobileVO, Technician and Service.  Technician to be used to add a new technicican.  Service to be used to create a service appointment. Service will reference both technician and automobileVO.

*Note for where I am in the project:

The inventory portion is complete.

My portion is not complete.  I was able to get the technician form up and working.  The service appointment form is up but isn't posting.  I feel like it has something to do with my views but i've just been stuck and unable to move forward.  

## Sales microservice

I am going to start with my models.  I feel like this gives me a solid foundation of the properties that I need to account for and just plants the tree for how they need to be used and what they will be used for.

Explain your models and integration with the inventory
microservice, here.
