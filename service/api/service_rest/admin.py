from django.contrib import admin
from .models import Technician

# Register your models here.
admin.site.register(Technician)
