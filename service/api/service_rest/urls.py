from django.urls import path
from service_rest.views import (
    api_technicians,
    api_technician,
    api_service_appointments)

urlpatterns = [
    path('technicians/', api_technicians, name="api_technicians",),
    path('technicians/<int:pk>/', api_technician, name="api_technician",),
    path('services/', api_service_appointments, name="api_service_appointments",),

    
]
