from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    href= models.CharField(max_length=200)
    vin = models.CharField(max_length=200)

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField()

class Service(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    appointment_date_time = models.DateTimeField(null=True, blank=True)
    reason= models.TextField()
    technician = models.ForeignKey(
        "Technician",
        related_name="appointment",
        on_delete=models.PROTECT,
    )
    is_vip=models.BooleanField(default=False)



