import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">List of manufacturers </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/delete">Remove manufacturers </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Create a vehicle model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">List of vehicle models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/delete">Remove vehicle model </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Create an automobile </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">List of automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/delete">Remove automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sale">Add a sales person</NavLink> //fix
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customer">Add A potential customer</NavLink> //fix
              <NavLink className="nav-link" to="/technicians/new">Add a technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/services/new">Service Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/services">List of Appointments</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
};

export default Nav;
