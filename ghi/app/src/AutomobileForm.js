import React from 'react';

class AutomobileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      color: "",
      year: "",
      models: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeYear = this.handleChangeYear.bind(this);
    this.handleChangeModel = this.handleChangeModel.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.models;

    const automobileURL = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(automobileURL, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);
      this.setState({
        vin: "",
        color: "",
        year: "",
        models: [],
      });
    }
  }

  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }
  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }
  handleChangeYear(event) {
    const value = event.target.value;
    this.setState({ year: value });
  }
  handleChangeModel(event) {
    const value = event.target.value;
    this.setState({ model_id: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add automobile</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="automobile">Vin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="automobile">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeYear} value={this.state.year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="automobile">Year</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeModel} value={this.state.model} required name="model" id="model" className="form-select">
                  <option value="">Choose Model</option>
                  {this.state.models.map(model => {
                    return(
                      <option key={model.href} value={model.id}>
                        {model.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;
