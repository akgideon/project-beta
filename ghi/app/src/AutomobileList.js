function AutomobileList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">List of Automobiles</h2>
        <div className="row">
          {props.automobiles.map(automobile => {
            return (
              <div key={automobile.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{automobile.vin}</h5>
                    <h6 className="care-title">{automobile.year}</h6>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default AutomobileList
