function VehicleModelList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">List of Vehicle Models</h2>
        <div className="row">
          {props.models.map(model => {
            return (
              <div key={model.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{model.name}</h5>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default VehicleModelList