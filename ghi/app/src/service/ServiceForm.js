import React from 'react';

class ServiceFrom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_name: "",
      vin:"",
      appointment_date_time: "",
      reason:"",
      technician:"",
      technicians: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeCustomerName = this.handleChangeCustomerName.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleChangeApptDateTime = this.handleChangeApptDateTime.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technicians;

    const serviceUrl = 'http://localhost:8080/api/services/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(serviceUrl, fetchConfig);
    if (response.ok) {
      const newService = await response.json();
      console.log(newService);
      this.setState({
        customer_name: "",
        vin: "",
        appointment_date_time: "",
        reason: "",
        technician: "",
      });
    }
  }

  handleChangeCustomerName(event) {
    const value = event.target.value;
    this.setState({ customer_name: value });
  }
  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }
  handleChangeApptDateTime(event) {
    const value = event.target.value;
    this.setState({ appointment_date_time: value });
  }
  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }
  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Service Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeCustomerName} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeApptDateTime} value={this.state.appointment_date_time} placeholder="Appointment Date and Time" required type="text" name="appt_date_time" id="appt_date_time" className="form-control" />
                <label htmlFor="appt_date_time">Date/Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="picture_url">reason</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={this.handleChangeTechnician} value={this.state.technician} placeholder="Technician" required type="text" name="technician" id="technician" className="form-select">
                  <option value="">Choose a Technician</option>
                    {this.state.technicians.map(technician =>{
                      return(
                        <option key={technician.id} value={technician.id}> {technician.name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceFrom;
