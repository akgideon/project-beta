function ServiceList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">List of Appointments</h2>
        <div className="row">
          {props.services.map(service => {
            return (
              <div key={service.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{service.vin}</h5>
                    <h6 className="care-title">{service.reason}</h6>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default ServiceList