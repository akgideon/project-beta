import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerDelete from './ManufacturerDelete';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelDelete from './VehicleModelDelete';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import AutomobileDelete from './AutomobileDelete';
// import SaleForm from './sale/Saleform';
// import SaleList from './sale/SaleList'
// import CustomerForm from './sale/CustomerForm';
// import CustomerList from './sale.CustomerList';
import TechnicianForm from './service/TechnicianForm';
import TechnicianList from './service/TechnicianList';
import ServiceForm from './service/ServiceForm';
import ServiceList from './service/ServiceList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={props.manufacturers} />}/>
            <Route path="new" element={<ManufacturerForm />}/>
            <Route path="delete" element={<ManufacturerDelete />}/>
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList models={props.models} />}/>
            <Route path="new" element={<VehicleModelForm />}/>
            <Route path="delete" element={<VehicleModelDelete />}/>
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList automobiles={props.automobiles} />}/>  
            <Route path="new" element={<AutomobileForm />}/>
            <Route path="delete" element={<AutomobileDelete />}/>
          </Route>
          {/* <Route path="sale">
            <Route path="new" element={<SaleForm/>} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm/>} />
            </Route> */}
          <Route path="technicians"> 
            <Route index element={ <TechnicianList technician={props.technicians} />}/>
            <Route path="new" element={<TechnicianForm />}/>
          </Route>
          <Route path="services"> 
          <Route index element={ <ServiceList service={props.services} />}/>
            <Route path="new" element={<ServiceForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
};

export default App;
