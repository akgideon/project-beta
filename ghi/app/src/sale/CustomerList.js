function CustomerList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">Customer List</h2>
        <div className="row">
          {props.customers.map(customer => {
            return (
              <div key={customer.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{customer.name}</h5>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default CustomerList
