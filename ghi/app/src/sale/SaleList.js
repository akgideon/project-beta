function SaleList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">Sales</h2>
        <div className="row">
          {props.sales.map(sale => {
            return (
              <div key={sale.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{sale.name}</h5>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default SaleList
