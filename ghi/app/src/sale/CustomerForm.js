import React from 'react';

class CustomerForm extends React.Component {
    constructor(props) {
        super (props);
        this.state = {
            customer_name: " ",
            customer_address: " ",
            customer_phone_number: " ",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeCustomerName = this.handleChangeCustomerName.bind(this);
        this.handleChangeCustomerAddress = this.handleChangeCustomerAddress.bind(this);
        this.handleChangeCustomerPhoneNumber = this.handleChangeCustomerPhoneNumber.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost8090/api/locations';
        
        const response = await fetch (url);

        if (response.ok) {
            const data = await response.json();
            this.setState ({customers: data.customers});
        }
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSales = await response.json();
            console.log(newSales);
            this.setState ({
                name: '',
                employee_number: '',
            });
        }
    }
handleChangeCustomerName(event){
    const value = event.target.value;
    this.setState({ customer_name: value});
}
handleChangeCustomerAddress(event){
    const value = event.target.value;
    this.setState({customer_address: value});
}
handleChangeCustomerPhoneNumber(event){
    const value = event.target.value;
    this.setState({customer_phone_number: value});
}
render(){
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadpw p-4 mt-4">
                    <h1>Add a potential customer</h1>
                    <form on Submit={this.handleSubmit} id="create-customer-form"></form>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangCustomerName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor ="name">Name</label>
                    </div>
                    <div className="form-floating mb-3"></div>
                        <input onChange={this.handleChangeCustomerAddress} value={this.state.name} placeholder="customer_address" required type="text" name="customer" id="customer_address" className="form-control" />
                        <label htmlFor="customer_address">Customer Address</label>
                    </div>
                    <div className="form-floating mb-3"></div>
                        <input onChange={this.handleChangeCustomerPhoneNumber} value={this.state.name} placeholder="customer_phone_number" required type="text" name="customer_phone_number" id="customer_phone_number" className="form-control" />
                        <label htmlFor="customer_phone_number">Customer Phone Number</label>
                <button className= "btn btn-primary">Add</button>
            </div>
        </div>
    );
}
}

export default CustomerForm;
