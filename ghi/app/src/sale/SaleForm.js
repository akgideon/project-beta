import React from 'react';

class SalesForm extends React.Component {
    constructor(props) {
        super (props);
        this.state = {
            employee_name: " ",
            vin_number: " ",
            sale_price: " ",
            

        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSales = await response.json();
            console.log(newSales);
            this.setState ({
                name: '',
                employee_number: '',
            });
        }
    }
handleChangeName(event){
    const value = event.target.value;
    this.setState({ name: value});
}
handleChangeEmployeeNumber(event){
    const value = event.target.value;
    this.setState({employee_number: value});
}

render(){
    return 
    
}
}

export default SalesForm;
