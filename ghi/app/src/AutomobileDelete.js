import React from 'react';
import { renderMatches } from 'react-router-dom';

class AutomobileDelete extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: []
};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAutomobileSelection = this.handleAutomobileSelection.bind(this);
}

async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        this.setState({automobiles: data.automobiles });
    }
}

async handleSubmit(event) {
    event.preventDefault();

    const automobileUrl = `http://localhost:8100/api/automobiles/${this.state.automobile}`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
};

const response = await fetch(automobileUrl, fetchConfig);
if (response.ok) {
    const newAutomobile = await response.json();
    console.log(newAutomobile)

    let remainingAutomobiles = []
    for (let i = 0; i < this.state.automobiles.length; i++){
        let currentAutomobile = this.state.automobiles[i]
        if (parseInt(this.state.automobile) !== currentAutomobile.id) {
            remainingAutomobiles.push(currentAutomobile)
        }
    }
    this.setState({
        automobile: '',
        automobiles: remainingAutomobiles
    })
}
}
handleAutomobileSelection(event) {
    const value = event.target.value;
    this.setState({automobile: value});
}

render(){
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Automobile To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-automobile-form">
              <div className="mb-3">
                <select onChange={this.handleAutomobileSelection} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose Automobiles</option>
                  {this.state.automobiles.map(automobile => {
                    return (
                      <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileDelete;
