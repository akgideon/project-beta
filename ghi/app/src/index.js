import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));


async function loadInventory() {
  let manufacturerData = [];
  let vehicleModelData =  [];
  let automobileData = [];
  let saleData = [];
  let customerData = [];
  let technicianData = [];
  let serviceData = [];
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const vehicleModelResponse = await fetch('http://localhost:8100/api/models');
  const saleResponse = await fetch ('http://localhost:8100/api/sales/');
  const customerResponse = await fetch ('http://localhost:8100/api/customers/');
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
  const serviceResponse = await fetch('http://localhost:8080/api/services/');
  console.log("automobile response", automobileResponse);

  

  if (manufacturerResponse.ok) {
    manufacturerData = await manufacturerResponse.json();
    manufacturerData = manufacturerData.manufacturers;
    console.log('manufacturer data: ', manufacturerData)
  } else {
    console.error(manufacturerResponse);
  }
  if (vehicleModelResponse.ok) {
    vehicleModelData = await vehicleModelResponse.json();
    vehicleModelData = vehicleModelData.models;
    console.log('vehicle model data: ', vehicleModelData)
  } else {
    console.error(vehicleModelResponse);
  }
  if (automobileResponse.ok) {
    automobileData = await automobileResponse.json();
    automobileData = automobileData.automobiles;
    console.log('automobile data: ', automobileData)
  } else {
    console.error(automobileResponse);
  }
  // if (saleResponse.ok) {
  //   saleData = await saleResponse.json();
  //   saleData = saleData.sale;
  //   console.log('Sale data: ', saleData)
  // } else {
  //   console.error(employeeResponse);
  // }
  if (customerResponse.ok) {
    customerData = await customerResponse.json();
    customerData = customerData.customer;
    console.log('customer data: ', customerData)
  } else {
    console.error(customerResponse);
  if (technicianResponse.ok) {
    technicianData = await technicianResponse.json();
    technicianData = technicianData.technicians;
    console.log('Technician data: ', technicianData)
  } else {
    console.error(technicianResponse);
  }
  if (serviceResponse.ok) {
    serviceData = await serviceResponse.json();
    serviceData = serviceData.services;
    console.log('Service data: ', serviceData)
  } else {
    console.error(serviceData);
  }

  root.render(
    <React.StrictMode>
      <App manufacturers={manufacturerData} models={vehicleModelData} automobiles={automobileData} sales={saleData} customers={customerData}  />
    </React.StrictMode>
  );
}
}
loadInventory();
