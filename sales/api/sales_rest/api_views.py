from msilib.schema import Class
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Sales
import json

class SalesDetailEncoder(ModelEncoder):
    model = Sales
    properties = [
        "sales_person",
        "customer_name",
        "vin_number",
        "sale_price"
    ]

