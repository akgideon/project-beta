from django.contrib import admin
from .models import Employee, Customer 

# Register your models here.
@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):  
    pass 

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass