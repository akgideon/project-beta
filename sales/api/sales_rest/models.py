from django.db import models


# Create your models here.

class AutomobileVO (models.Model):
    color = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    vin = models.PositiveBigIntegerField(unique=True)

class Customer (models.Model):
    customer_name = models.CharField(max_length=200)
    customer_address = models.CharField(max_length=500)
    customer_phone_number = models.SmallIntegerField 

class Employee (models.Model):
    employee_name = models.CharField(max_length = 200)
    employee_id = models.PositiveSmallIntegerField

class Sale (models.Model):
    employee_name = models.CharField(max_length=200)
    vin_number = models.PositiveBigIntegerField(unique=True)
    sale_price = models.PositiveBigIntegerField()
    customer = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )


