from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Sale, Customer, AutomobileVO  
from common.json import ModelEncoder
import json

# Create your views here.
class CustomerDetailEncoder (ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "customer_address",
        "customer_phone_number"
    ]

class SaleDetailEncoder (ModelEncoder):
    model = Sale
    properies = [
        "employee_name",
        "vin_number",
        "sale_price"
    ]
    encoders = {
        "customer": CustomerDetailEncoder(),
    }

class SaleListEncoder (ModelEncoder):
    model = Sale

@require_http_methods(["GET", "POST"])
def api_sale(request, pk):
    if request.method == "GET":
        sales = Sale.objects.all(id=pk)
        return JsonResponse(
            sales,
            encoder=SaleDetailEncoder,
            safe=False
        )
    else: 
        content = json.loads(request.body)
    
        try:
            sale = Sale.object.all(id=pk)

        except Sale.DoesNotExist:
            return JsonResponse (
                {"message": 'No sale exist!'},
                status = 400
            )

@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customers = Customer.object.all(id=pk)
        return JsonResponse(
            customers,
            encoder=CustomerDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse (
                {"message": 'No customer exist!'},
                status = 400
            )
        customer = Customer.objects.create(**content)
        return JsonResponse (
            customer,
            encoder = CustomerDetailEncoder,
            safe = False,
        )        

